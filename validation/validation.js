// Kiểm tra định dạng số
function kiemTraSo(value, selectorError, name, min, max) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value) && value.length >= min && value.length <= max) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + `, tất cả là số từ ${min} đến ${max} kí tự`;
    return false;
  }
}
// Kiểm tra định dạng chữ
function kiemTraChu(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z]+$/;
  if (value.length == 0) {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống.";
    return false;
  } else if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + ", tất cả phải là chữ cái không dấu.";
    return false;
  }
}
// Kiểm tra định dạng email
function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (value.length == 0) {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống.";
    return false;
  } else if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + " phải đúng định dạng.";
    return false;
  }
}
// Kiểm tra định dạng password 6-10 kí tự(1 ký tự số, 1 in hoa, 1 đặc biệt)
function kiemTraPassword(value, selectorError, name) {
  var regexPassword =
    /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{6,10}$/;

  if (value.length < 6 || value.length > 10) {
    document.querySelector(selectorError).innerHTML = name + " từ 6-10 kí tự";
    return false;
  } else if (regexPassword.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + " gồm ít nhất 1 kí tự số, 1 in hoa, 1 kí tự đặc biệt.";
    return false;
  }
}
// Kiểm tra định dạng ngày tháng
function kiemTraDate(value, selectorError, name) {
  var regexDate = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

  if (value.length == 0) {
    document.querySelector(selectorError).innerHTML = name + " không bỏ trống";
    return false;
  } else if (regexDate.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + " nhập đúng định dạng mm/dd/yyyy";
    return false;
  }
}
// Kiểm tra định dạng lương cơ bản và số giờ làm
function kiemTraLuong(value, selectorError, name, min, max) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value) && value * 1 >= min && value * 1 <= max) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + `  trong khoảng ${min.toLocaleString()} - ${max.toLocaleString()}`;
    return false;
  }
}
// Kiểm tra trùng tài khoản
function loopAccount(data,value,selectorError,name){
    var index = data.findIndex(function (item) {
        return item.account == value;
      });
      if(index != -1){
        document.querySelector(selectorError).innerHTML = name + " đã tồn tại.";
        return false;
      }
      else{
        document.querySelector(selectorError).innerHTML = "";
        return true;
      }
}
// Kiểm tra trùng email
function loopEmail(data,value,selectorError,name){
    var index = data.findIndex(function (item) {
        return item.email == value;
      });
      if(index != -1){
        document.querySelector(selectorError).innerHTML = name + " đã tồn tại.";
        return false;
      }
      else{
        document.querySelector(selectorError).innerHTML = "";
        return true;
      }
   
}

