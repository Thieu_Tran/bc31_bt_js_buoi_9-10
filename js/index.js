// Mảng chứa danh sách nhân viên
var arrDSNV = [];

// Lấy dữ liệu từ localStorage gán vào arr
var dataJson = localStorage.getItem("DSNV");
if (dataJson != null) {
  var jsonToArr = JSON.parse(dataJson);

  for (var index = 0; index < jsonToArr.length; index++) {
    var item = jsonToArr[index];
    var nv = new NhanVien(
      item.account,
      item.name,
      item.email,
      item.password,
      item.date,
      item.salary,
      item.position,
      item.time
    );
    arrDSNV.push(nv);
  }
  renderDSNV(arrDSNV);
}
// function thêm nhân viên vào mảng
function addNhanVien() {
  var nv = layThongTinTuForm();

  // Kiểm tra dữ liệu nhập vào
  var validation = true;

  validation = kiemTraSo(nv.account, "#tbTKNV", "Tài khoản", 4, 6);
  validation &= kiemTraChu(nv.name, "#tbTen", "Tên");
  validation &= kiemTraEmail(nv.email, "#tbEmail", "Email");
  validation &= kiemTraPassword(nv.password, "#tbMatKhau", "Mật khẩu");
  validation &= kiemTraDate(nv.date, "#tbNgay", "Ngày tháng");
  validation &= kiemTraLuong(
    nv.salary,
    "#tbLuongCB",
    "Lương cơ bản",
    1000000,
    20000000
  );
  validation &= kiemTraLuong(nv.time, "#tbGiolam", "Số giờ", 80, 200);
  // Kiểm tra nhập chức vụ
  if (nv.position == "Chọn chức vụ") {
    document.querySelector("#tbChucVu").innerHTML =
      "Vui lòng chọn đúng chức vụ.";
    return (validation = false);
  } else {
    document.querySelector("#tbChucVu").innerHTML = "";
  }
  // Kiểm tra trùng lặp dữ liệu
  validation &= loopAccount(arrDSNV, nv.account, "#tbTKNVLoop", "Tài khoản");
  validation &= loopEmail(arrDSNV, nv.email, "#tbEmailLoop", "Email");

  if (!validation) {
    return;
  }
  arrDSNV.push(nv);

  renderDSNV(arrDSNV);

  var dsnvJson = JSON.stringify(arrDSNV);
  localStorage.setItem("DSNV", dsnvJson);
}
// function xóa nhân viên
function deleteNV(account) {
  // Lấy vị trí của nhân viên trong danh sách cần xóa
  var index = arrDSNV.findIndex(function (item) {
    return item.account == account;
  });
  arrDSNV.splice(index, 1);
  renderDSNV(arrDSNV);
  var dsnvJson = JSON.stringify(arrDSNV);
  localStorage.setItem("DSNV", dsnvJson);
}

// function cập nhật nhân viên
function fixNV(account) {
  // Khóa button thêm người dùng vì đang trong form cập nhật.
  document.getElementById("btnThemNV").disabled = true;
  var currentNV = "";
  var index = arrDSNV.findIndex(function (item) {
    return item.account == account;
  });
  currentNV = arrDSNV[index];

  dataToForm(currentNV);
}
function update() {
  var nv = layThongTinTuForm();
  // Lấy vị trí của nhân viên cần update dữ liệu, sử dụng để check email.
  var index = arrDSNV.findIndex(function (item) {
    return item.account == nv.account;
  });
// Kiểm tra dữ liệu nhập vào
  var validation = true;
  validation = kiemTraChu(nv.name, "#tbTen", "Tên");
  validation &= kiemTraEmail(nv.email, "#tbEmail", "Email");
  validation &= kiemTraPassword(nv.password, "#tbMatKhau", "Mật khẩu");
  validation &= kiemTraDate(nv.date, "#tbNgay", "Ngày tháng");
  validation &= kiemTraLuong(
    nv.salary,
    "#tbLuongCB",
    "Lương cơ bản",
    1000000,
    20000000
  );
  validation &= kiemTraLuong(nv.time, "#tbGiolam", "Số giờ", 80, 200);
   // Kiểm tra nhập chức vụ
   if (nv.position == "Chọn chức vụ") {
    document.querySelector("#tbChucVu").innerHTML =
      "Vui lòng chọn đúng chức vụ.";
    return (validation = false);
  } else {
    document.querySelector("#tbChucVu").innerHTML = "";
  }
  // Kiểm tra trùng email, bỏ qua trùng lặp index hiện tại
  for (var i = 0; i < arrDSNV.length; i++) {
    if (i != index && arrDSNV[i].email == nv.email) {
      document.querySelector("#tbEmailLoop").innerHTML = "Email đã tồn tại.";
      return (validation = false);
    } else {
      document.querySelector("#tbEmailLoop").innerHTML = "";
    }
  }

  if (!validation) {
    return;
  }
    currentNV = document.getElementById("tknv").value;
    for (var index = 0; index < arrDSNV.length; index++) {
      if (currentNV == arrDSNV[index].account) {
        arrDSNV[index] = layThongTinTuForm();
      }
    }
    renderDSNV(arrDSNV);
    var dsnvJson = JSON.stringify(arrDSNV);
    localStorage.setItem("DSNV", dsnvJson);
}
// function lọc nhân viên theo xếp loại
function search() {
  var arrFilter = [];
  var strFilter = document.getElementById("searchName").value;
  for (var index = 0; index < arrDSNV.length; index++) {
    if (arrDSNV[index].xepLoai() == strFilter) {
      arrFilter.push(arrDSNV[index]);
    }
  }
  if (arrFilter.length != 0) {
    renderDSNV(arrFilter);
  }
}
