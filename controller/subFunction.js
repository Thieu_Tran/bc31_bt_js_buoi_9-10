// Tạo lớp đối tượng
function NhanVien(
  _account,
  _name,
  _email,
  _password,
  _date,
  _salary,
  _position,
  _time
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.date = _date;
  this.salary = _salary;
  this.position = _position;
  this.time = _time;
  this.tongLuong = function () {
    var totalSalary = "";
    if (this.position == "Sếp") {
      return (totalSalary = (this.salary * 3).toLocaleString());
    } else if (this.position == "Trưởng phòng") {
      return (totalSalary = (this.salary * 2).toLocaleString());
    } else {
      return (totalSalary = this.salary.toLocaleString());
    }
  };
  this.xepLoai = function () {
    if (this.time >= 192) {
      return "Xuất sắc";
    } else if (this.time >= 176) {
      return "Giỏi";
    } else if (this.time >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}

// Mở khóa button thêm nhân viên
function able() {
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").disabled = false;

  // reset form
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("gioLam").value = "";

  var resetSpan = document.querySelectorAll(".sp-thongbao");
  for(index = 0;index<resetSpan.length;index++){
    resetSpan[index].innerHTML ="";
  }

}
// function lấy thông tin từ form
var layThongTinTuForm = function () {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var salary = document.getElementById("luongCB").value * 1;
  var position = document.getElementById("chucvu").value;
  var time = document.getElementById("gioLam").value * 1;

  var nv = new NhanVien(
    account,
    name,
    email,
    password,
    date,
    salary,
    position,
    time
  );
  return nv;
};
// Lấy dữ liệu nhân viên hiển thị lên form
function dataToForm(nv) {
  var resetSpan = document.querySelectorAll(".sp-thongbao");
  for(index = 0;index<resetSpan.length;index++){
    resetSpan[index].innerHTML ="";
  }
  
  document.getElementById("tknv").value = nv.account;
  document.getElementById("tknv").disabled = true;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.date;
  document.getElementById("luongCB").value = nv.salary;
  document.getElementById("chucvu").value = nv.position;
  document.getElementById("gioLam").value = nv.time;
}
// function render giao diện
function renderDSNV(array) {
  var content = "";
  for (var i = 0; i < array.length; i++) {
    var nv = array[i];
    var contentTr = `<tr>
                              <td>${nv.account}</td>
                              <td>${nv.name}</td>
                              <td>${nv.email}</td>
                              <td>${nv.date}</td>
                              <td>${nv.position}</td>
                              <td>${nv.tongLuong()}</td>
                              <td>${nv.xepLoai()}</td>
                              <td class="d-flex">
                                <button class="btn btn-primary  py-1" data-toggle="modal" data-target="#myModal" onclick="fixNV('${
                                  nv.account
                                }')">Sửa</button>
                                <button class="btn btn-danger py-1" onclick="deleteNV('${
                                  nv.account
                                }')">X</button>
                              </td>
                          </tr>
                          `;
    content += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = content;
}
